const DNAStrand = (dna) => {
    const complements = {
        'A': 'T',
        'T': 'A',
        'C': 'G',
        'G': 'C'
    };
    
    return dna.split('').map(nucleotide => complements[nucleotide]).join('');
};

console.log(DNAStrand("ATTGC")); // Output: "TAACG"
console.log(DNAStrand("GTAT")); // Output: "CATA"
