const maskify = (cc) => 
    cc.length <= 4 
        ? cc 
        : '#'.repeat(cc.length - 4) + cc.slice(-4);

console.log(maskify('4556364607935616'));
console.log(maskify('1'));
console.log(maskify('11111'));
