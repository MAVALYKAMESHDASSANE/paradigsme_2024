package Paradigmes;

	interface Empruntable {
	    void emprunter();
	    void retourner();
	}
