package Paradigmes;


class Membre extends Personne {
 private String dateAdhesion; 
 private String statut;
 private Livre[] emprunts;
 private int nbEmprunts;


 public Membre(String nom, String prenom, int id, String dateAdhesion, String statut) {
     super(nom, prenom, id);
     this.dateAdhesion = dateAdhesion;
     this.statut = statut;
     this.emprunts = new Livre[10]; 
     this.nbEmprunts = 0;
 }


 public String getDateAdhesion() {
     return dateAdhesion;
 }

 public void setDateAdhesion(String dateAdhesion) {
     this.dateAdhesion = dateAdhesion;
 }

 public String getStatut() {
     return statut;
 }

 public void setStatut(String statut) {
     this.statut = statut;
 }

 public Livre[] getEmprunts() {
     return emprunts;
 }

 public int getNbEmprunts() {
     return nbEmprunts;
 }

 public void emprunter(Livre livre) {
     if (livre.isDisponible() && nbEmprunts < emprunts.length) {
         livre.emprunter();
         emprunts[nbEmprunts++] = livre;
     } else {
         System.out.println("Le livre " + livre.getTitre() + " n'est pas disponible ");
     }
 }


 public void retourner(Livre livre) {
     for (int i = 0; i < nbEmprunts; i++) {
         if (emprunts[i] == livre) {
             livre.retourner();
             emprunts[i] = emprunts[--nbEmprunts]; 
             emprunts[nbEmprunts] = null; 
             return;
         }
     }
     System.out.println("Le livre " + livre.getTitre() + " n'est pas dans la liste des emprunts.");
 }

 public void afficherDetails() {
     System.out.println("Membre [ID=" + getId() + ", Nom=" + getNom() + ", Prénom=" + getPrenom() +
                        ", Date d'Adhésion=" + dateAdhesion + ", Statut=" + statut + "]");
     System.out.println("Livres empruntés:");
     for (int i = 0; i < nbEmprunts; i++) {
         System.out.println(" - " + emprunts[i].getTitre());
     }
 }


}
