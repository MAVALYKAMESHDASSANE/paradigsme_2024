package Paradigmes;

public class Main {
    public static void main(String[] args) {
        // Création des objets Membre et Employe
        Membre membre1 = new Membre("MAVALY", "Kamesh", 1, "2022-05-14", "Actif");
        Employe employe1 = new Employe("MANE", "COLY", 2, "Bibliothécaire", 2500.00);
        Membre membre2 = new Membre("MANGENOT", "Ludovic", 3, "2022-06-01", "Actif");

        // Création de quelques livres
        Livre livre1 = new Livre("Les Misérables", "Victor Hugo", "1234567890", true);
        Livre livre2 = new Livre("1984", "George Orwell", "0987654321", true);
        Livre livre3 = new Livre("Carbonne & Siliciome", "Mathieu Bablet", "979-1033511960", true);

        Personne[] personnes = {membre1, employe1, membre2};

        for (Personne personne : personnes) {
            personne.afficherDetails();
        }

        membre1.emprunter(livre1);
        membre1.emprunter(livre2);
        membre2.emprunter(livre3);

   
        membre1.afficherDetails();

        membre1.retourner(livre1);
        membre1.retourner(livre3);

        membre2.afficherDetails();
    }
}

